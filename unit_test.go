package mlf2sexp

import (
	"fmt"
	"strings"
	"testing"

	"github.com/nsf/sexp"
)

func TestBasic(t *testing.T) {
	table := []struct {
		in, name string
	}{
		{
			in:   "(optode #5b44fe7b# |Qg0CDECE9cNErHZmRGEgAELHfO5Bl2Ra|)",
			name: "optode",
		},
		{
			in:   "(flntu #5b44fe7d# |ArcAWwK8ECI=|)",
			name: "flntu",
		},
		{
			in:   "(cstar #5b44fe8b# #ff4#)",
			name: "cstar",
		},
	}

	for _, e := range table {
		rec := Record{}
		ast, err := sexp.Parse(strings.NewReader(e.in), nil)
		if err != nil {
			t.Fatal(err)
		}
		err = ast.Children.Unmarshal(&rec)
		if err != nil {
			t.Fatal(err)
		}

		if rec.Sensor != e.name {
			t.Errorf("Decoding error; expected %q, got %q",
				e.name, rec.Sensor)
		}

		if rec.Time().IsZero() {
			t.Error("Timestamp not decoded")
		}
	}
}

func TestIntDecode(t *testing.T) {
	input := "(cstar #5b44fe8b# #ff4#)"
	rec := Record{}
	ast, err := sexp.Parse(strings.NewReader(input), nil)
	if err != nil {
		t.Fatal(err)
	}
	err = ast.Children.Unmarshal(&rec)
	if err != nil {
		t.Fatal(err)
	}

	var i Int
	err = rec.ScanData(&i)
	if err != nil {
		t.Fatal(err)
	}

	if i != 0xff4 {
		t.Errorf("Decode error; expected 0xff4, got %#x", i)
	}
}

func TestListDecode(t *testing.T) {
	input := "(ctdo #5abdf6fe# (|QRvvNUHcmwkAAAAA| #1#))"
	rec := Record{}
	ast, err := sexp.Parse(strings.NewReader(input), nil)
	if err != nil {
		t.Fatal(err)
	}
	err = ast.Children.Unmarshal(&rec)
	if err != nil {
		t.Fatal(err)
	}

	var (
		b     Bytes
		state Int
	)

	if n := rec.Len(); n != 2 {
		t.Errorf("Incorrect length; expected 2, got %d", n)
	}

	err = rec.ScanData(&b, &state)
	if err != nil {
		t.Fatal(err)
	}

	if state != 1 {
		t.Errorf("Decode error; expected 1, got %v", state)
	}
}

func TestBinaryDecode(t *testing.T) {
	table := []struct {
		in   string
		size int
	}{
		{
			in:   "(flntu #5b44fe7d# |ArcAWwK8ECI=|)",
			size: 8,
		},
	}

	for _, e := range table {
		rec := Record{}
		ast, err := sexp.Parse(strings.NewReader(e.in), nil)
		if err != nil {
			t.Fatal(err)
		}
		err = ast.Children.Unmarshal(&rec)
		if err != nil {
			t.Fatal(err)
		}

		var b Bytes
		err = rec.ScanData(&b)
		if err != nil {
			t.Fatal(err)
		}

		if len(b) != e.size {
			t.Errorf("Wrong data length; expected %d, got %d",
				e.size, len(b))
		}
	}
}

const MULTI = `(fpr #55f0e2dc# (|Pk1PjT5YMuw+T1QhPjRMIj5Z1qQ+QqH+PjB4yT5QVms+R8L2Pljp4T4E604+U0fDPh/oij44CfU+QVRePlqjHz5HrXA+VKrpPi9rvD5T8/U+SlNyPjmCoA==| |Px7uXz8fnsw/FwQkPx1tcT8bvGU/FkkGPxhU9T8dJ/E/His8Px5TVT8Xny8/HMe1PxkoIj8XBCQ/Gx6uPyE6dj8cXMg/G5b5PxouGT8dDTY/GaBsPxfXUQ==| |Px77fj8df+Y/FcBhPx3tgD8Xdsk/FfiEPxmgIz8iODA/Iss3PyF/vD8dyr8/IXe3Px3oJz8bG7w/ITzoPyYCkD8evgI/IMn1Px7LYD8hV6M/HYU+PxeW3Q==| ))`

func TestDataList(t *testing.T) {
	rec := Record{}
	ast, err := sexp.Parse(strings.NewReader(MULTI), nil)

	if err != nil {
		t.Fatal(err)
	}
	err = ast.Children.Unmarshal(&rec)
	if err != nil {
		t.Fatal(err)
	}

	if !rec.HasList() {
		t.Errorf("List data not detected")
	}

	if n := rec.Len(); n != 3 {
		t.Errorf("Incorrect length; expected 3, got %d", n)
	}

	//var b [3]Bytes
	b := make([]Bytes, 3)

	err = rec.ScanData(&b)
	if err != nil {
		t.Fatal(err)
	}

	n0, n1, n2 := len(b[0]), len(b[1]), len(b[2])
	if n0 == 0 {
		t.Fatal("Data size is zero")
	}

	if n0 != n1 || n1 != n2 {
		t.Errorf("Length mismatch: %d, %d, %d", n0, n1, n2)
	}
}

const SINGLE = `(fpr #55f0e2dc# (|Pk1PjT5YMuw+T1QhPjRMIj5Z1qQ+QqH+PjB4yT5QVms+R8L2Pljp4T4E604+U0fDPh/oij44CfU+QVRePlqjHz5HrXA+VKrpPi9rvD5T8/U+SlNyPjmCoA==|))`

func TestSingleElem(t *testing.T) {
	rec := Record{}
	ast, err := sexp.Parse(strings.NewReader(SINGLE), nil)

	if err != nil {
		t.Fatal(err)
	}
	err = ast.Children.Unmarshal(&rec)
	if err != nil {
		t.Fatal(err)
	}

	if !rec.HasList() {
		t.Errorf("List data not detected")
	}

	if n := rec.Len(); n != 1 {
		t.Errorf("Incorrect length; expected 1, got %d", n)
	}

	//var b [3]Bytes
	b := make([]Bytes, 1)

	err = rec.ScanData(&b)
	if err != nil {
		t.Fatal(err)
	}

	n0 := len(b[0])
	if n0 == 0 {
		t.Fatal("Data size is zero")
	}

}

const SUNA = `(suna #60929f25# (#3# |U0FUU0RCMDA5NkHhgAAAAAAAAAAAAAAAAABBUQAAQUEAAAAFYjI8znTSQT6kAECwyNdBWeAAAmYCZgJWAkUCTwJeAmsCVAJXAkECWQJaAmECYwJnAmACTwJrAlcCWwJZAlQCXQJbAlkCbAJVAnUCXQJnAl8CdQJpAksCXQJeAksCVwJnAoACXQJTAlECawJnAkgCVAJXAnACdQJGAlcCYQJNAmcCVwJrAmICYgJWAmUCZAJMAmUCZwJpAmoCUQJPAlQCUgJiAmUCewJzAmUCXgI9AnECXQJMAmYCWQJnAm8CkQJnAm0CawJxAk8CTwJqAnEChgJxAl4CVgJtAmgCXQKCAkUCZwJpAnACdwJ9AmICfgJ5Am8CbwJVAmECfwJyAnICXgJ3AmMCeAJQAnwCdwJ4AnMCaQJjAoECZgJ5AoMCdgJnAmkCYwKFAmwCQAJsAocCfwJ3AmICWQJ3AnECZwJxAmsCYQJ3AlUCXAJ3AmUCcAJ4AmoCagJrAmICeQJnAkcCeQJzAnQCaAJdAnkCgAJnAnACeAI0AmsCbgJlAnMCcwJSAncCWwJ5AnwCaQJZAm0CbQJhAl0CRwJoAmsCagJ3AnICZwJ/AmUCXwJ3AlkCbQJrAmoCRQJNAm0CeAJwAk4CVQJrAnkClwKAAlECXgJrAngCggJtAl8CdAJl4Q==| |U0FUU0xCMDA5NkHvwABBCL9ZPfUrMTZOdnhBUQAAQUEAAAAFYjI8znTSQT8eUUCsAa9BTmhsQmECZgKCAmkCbAJnAnEClQKAAo8ChgKVAmICaQJ/Ao4CmgKQAoUCmwJgAp8CkAK0AtYDBwO9BNIGiQltDbUTJhoGIqgswTgRRIVRxV+MbXV7PYg9lB2ebaZFq82uYa5eq7unnKJxnPSXi5L8j2iM7YtPioeK1IvKjSmPv5IolWGZPZ0WoRelLqjnrIWvebFisguxZa91q6umx6BNmS2Ri4lwga56PXNubSFnp2LUXqZbL1h7VhtUUFLzUiFRl1GlUYpSXFNtVJFVwVc3WNZaRVv3XSteEF75Xype2l4TXMda5FjeVk5TRFCNTWVKh0fXRQhCm0BLPmc8sDtfOiA5GThbN/o3qjdhN083dze7OAo4Vji8OW06HTqzO2M8DTzRPc0+ij8YP7pAOUDaQW5BhkG6QcFB10GBQQ1AZj+rPqo9izxDOsc5WTfkNkg0uDMAMWcvxS4pLGkqwCk5J6YmISSzI2UiDiD/H8se/x4fHWkcqhvWG1ca3RorGbUZNRjBGAsXuRcXFtQWbhYCFZ4VPhTTFJ0UHRPxE90TghNrE2UTARLDEpsSaRInEfcRvhGdEXUREBD+EPQQwhCCEAEP7Q+hD14PBQ7RYA==|))`

func TestSuna(t *testing.T) {
	rec := Record{}
	ast, err := sexp.Parse(strings.NewReader(SUNA), nil)

	if err != nil {
		t.Fatal(err)
	}
	err = ast.Children.Unmarshal(&rec)
	if err != nil {
		t.Fatal(err)
	}

	if !rec.HasList() {
		t.Errorf("List data not detected")
	}

	if n := rec.Len(); n != 3 {
		t.Errorf("Incorrect length; expected 3, got %d", n)
	}

	var mask Int
	b := make([]Bytes, rec.Len()-1)
	err = rec.ScanData(&mask, &b[0], &b[1])
	if err != nil {
		t.Fatalf("Data scan failed: %v", err)
	}

	if int(mask) != 3 {
		t.Errorf("Bad mask value; expected 3, got %d", mask)
	}

	if len(b[0]) == 0 {
		t.Errorf("Bad data length: %d", len(b[0]))
	}
	t.Logf("record length: %d bytes", len(b[0]))

	if len(b[0]) != len(b[1]) {
		t.Errorf("Data record lengths do not match")
	}
}

func ExampleScan() {
	input := `(optode #5b44fe7b# |Qg0CDECE9cNErHZmRGEgAELHfO5Bl2Ra|)
(flntu #5b44fe7d# |ArcAWwK8ECI=|)
(cstar #5b44fe8b# #ff4#)
\x1a\x1a\x1a(cstar #5b44fe8b# #ff4#)`
	s := NewScanner(strings.NewReader(input))
	for s.Scan() {
		rec := s.Record()
		fmt.Printf("%s %s\n", rec.Sensor, rec.Time())
	}
	// Output:
	// optode 2018-07-10 18:44:11 +0000 UTC
	// flntu 2018-07-10 18:44:13 +0000 UTC
	// cstar 2018-07-10 18:44:27 +0000 UTC
}
